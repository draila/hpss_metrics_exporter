# Export HPSS Metrics

[Mtail](https://github.com/google/mtail) program that gathers HPSS events/alarms/errors as metrics for Prometheus TSDB.

**NCSA Storage Enabling Technologies (SET) Group: set@ncsa.illinois.edu**

**Author: raila@illinois.edu**


**hpss.mtail** - The mtail spec for scraping hpss metrics

**deploy/** - Suggestions for deploying on init/systemd systems - customize to your deployment